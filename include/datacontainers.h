#ifndef DATACONTAINERS__H
#define DATACONTAINERS__H

#include <typedef.h>
#include <iostream>
#include <opencv2/core.hpp>
#include <map>
#include <vector>

//--------------------------------------Define containers for data, to use in the pipeline----------------------------------

struct ColorSegmentationResult {
	cv::Mat orig_img;
	cv::Mat segmented_img;
};


struct SignClassificationResult{
	cv::Mat orig_img;

#ifdef DEBUG
	cv::Mat segmented_image;
#endif // DEBUG
	
	std::map< std::string, std::vector<cv::Rect>> ROIs;
};

#endif