#ifndef PIPELINE__H
#define PIPELINE__H

#include <typedef.h>
#include <Windows.h>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <ctime>

/*
struct StageActions {
	std::string name;
	//ptr<void> ref_obj = nullptr; //optional

	std::function<ptr<void> ()> onStageInit = nullptr; //return this stage's data

	//get this stage 's data and the output of the previous stage as input, return the ouput of this stage.
	//Use the flag reached_end_of_input_data to signal the pipeline to stop, since this stage will never receive any more input data.
	//This feature is likely to be used by the first stage only.
	std::function<ptr<void> (ptr<void> prev_stage_output, ptr<void> this_stage_data, bool& reached_end_of_input)> onStageExecute; 
	
	std::function<void (ptr<void> this_stage_data)> onStageDealloc = nullptr; //deallocate this stage's data
};
*/

class Tic {
private:
	std::clock_t start;
public:
	inline Tic() { start = clock(); }
	inline double toc() { return double(clock() - start) / CLOCKS_PER_SEC; }
};

struct StageActions {
	std::string stage_name = "default stage";
	ptr<void> external_utility_data = nullptr; //store a reference to external data
	virtual void init() { return; }; //use to initialize some internal data with external info that where not available at contruction time of this object
	virtual ptr<void> execute(ptr<void> prev_stage_data, bool& reached_end_of_input) = 0 { return nullptr; };
	virtual void deallocate() { return; }; //use to deallocate external_utility_data, if needed
};

class Pipeline {

private:
	class Stage {
	private:
		ptr<void> indata;
		ptr<void> outdata;
		//ptr<void> stage_data;
		ptr<StageActions> a;
		bool done = true, must_join = false, is_dead = false;
		std::mutex mtx;
		std::condition_variable cv;
		std::thread t;
		std::function<void ()> stopPipeline;
#ifdef TIMER_ON
		double accumulator = 0, max_time = 0, min_time = 1e10;
		int toc_count = 0;
#endif //TIMER_ON


	public:
		std::string name;

		Stage(ptr<StageActions> a, std::function<void()> stopPipeline);
		Stage(const Stage& s);
		~Stage();

		void initResources(); //prepares data, if needed
		void execute(); //outputs elaborated data from input data (input data can be nullptr)
		void deallocateResources(); //deallocates resources, if needed

		ptr<void> retrieveOutputData();
		void receiveInputData(ptr<void> prev_stage_data);

		bool isDone() { return done; }
		bool isAlive() { return !is_dead; }
		void signalWork();
	};

public:
	Pipeline(int Hz = 30);
	~Pipeline();

	bool appendStageAt(ptr<StageActions> a, int idx = -1);
	bool removeStageAt(int idx);
	void run();	 //runs a new pipeline
	void stop(); //continue execution until also the last input has gone through the entire pipeline, then stops completely.
	bool isRunning() { return running; }
	std::string toString();

private:
	long msecs;
	bool running = false, paused = false;
	volatile bool shut_down = false;
	std::vector<Stage*> pl;
#ifdef TIMER_ON
	Tic t;
#endif // TIMER_ON
};

#endif
