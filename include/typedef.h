#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

#ifdef _DEBUG //defined by Visual Studio
#define DEBUG //Inherit from VS, but allow to disable this state even without switching to Release. Simply undef _DEBUG breaks VS.
#endif // _DEBUG

#define TIMER_ON

#include <iostream>

#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaobjdetect.hpp>

#if defined(HAVE_OPENCV_CUDAIMGPROC) && defined(HAVE_OPENCV_CUDAOBJDETECT)
#define CUDA_ON
#endif

template <class T>
using ptr = std::shared_ptr<T>; //alias for shared_ptr

#endif //_TYPEDEF_H_