#ifndef _STAGES_H_
#define _STAGES_H_

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/ml.hpp>

#include <iostream>
#include <random>
#include <map>
#include <vector>
#include <thread>

#include <typedef.h>
#include <pipeline.h>
#include <datacontainers.h>


//------------------------------------------------ Input stage -------------------------------------------------------
class ColorSegmentation : public StageActions {
private:
	cv::VideoCapture cap;

public:
	ColorSegmentation(std::string cfg_file);
	~ColorSegmentation();
	ptr<void> execute(ptr<void> prev_stage_data, bool& end_of_data) override;
};


//------------------------------------------------ Output stage ------------------------------------------------------
class SaveResults : public StageActions {
private:
	cv::VideoWriter vw;

#ifdef DEBUG
	cv::VideoWriter seg;
#endif // DEBUG
	int counter;
	std::string pattern;
	std::vector<cv::Scalar> colors;
	bool isVideo;

public:
	SaveResults(std::string cfg_file);
	~SaveResults();
	ptr<void> execute(ptr<void> prev_stage_data, bool& end_of_data) override;
};

//---------------------------------------- Sign Classification stage --------------------------------------------------
class SignClassifier : public StageActions {

private:
	class helper {
	private:
		cv::CascadeClassifier c;
		float scaleF;
		int minN, flags;
		cv::Size minS, maxS;
		void parseParams(const std::string& p);

	public:
		const std::string name;
		helper(std::string& xml, std::string& name, std::string& params);
		inline void detect(const cv::Mat& img, std::vector<cv::Rect>* objs) { c.detectMultiScale(img, *objs, scaleF, minN, flags, minS, maxS); }
	};

	std::vector<helper> classifiers;


public:
	SignClassifier(std::string config_file);

	ptr<void> execute(ptr<void> prev_stage_output, bool& reached_end_of_input) override;
};

//--------------------------------------- Digit Classification stage -------------------------------------------------

class DigitClassifier : public StageActions {
private:
	cv::Ptr<cv::ml::SVM> svm_digits;
	cv::Ptr<cv::MSER> mser;
	cv::HOGDescriptor hog;
	std::string look_for;

	cv::Rect find_digits_bb(cv::Size img_size, std::vector<cv::Rect> all_bb, float delta = 0.3);

public:
	DigitClassifier(std::string cfg_file);
	ptr<void> execute(ptr<void> prev_stage_output, bool& end_of_data) override;
};

#endif