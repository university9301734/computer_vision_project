#include <pipeline.h>

using namespace std;

Pipeline::Pipeline(int Hz) :
	msecs((long)floor(1000.f / Hz)),
	running(false),
	paused(false),
	shut_down(false)
{
#ifdef TIMER_ON
	t = Tic();
#endif // TIMER_ON
}

Pipeline::~Pipeline()
{
	if (running)
		stop();

	for (Stage* s : pl)
		delete s;

#ifdef TIMER_ON
	double time = t.toc();
	cout << endl << "PIPELINE TOTAL TIME: " << time << endl;
#endif // TIMER_ON
}

bool Pipeline::appendStageAt(ptr<StageActions> s, int idx)
{
	if (running)
		return false;

	auto f = std::bind(&Pipeline::stop, this);

	if (idx < 0 || idx >= pl.size())
		pl.push_back(new Pipeline::Stage(s, f));
	else
		pl.emplace(pl.begin() + idx, new Pipeline::Stage(s, f));

	return true;
}

bool Pipeline::removeStageAt(int idx)
{
	if (running)
		return false;
	if (idx < 0 || idx >= pl.size())
		return false;

	delete pl[idx]; //deallocate pointer
	pl.erase(pl.begin() + idx);
	return true;
}

void Pipeline::run()
{
	running = true;
	for (Stage* s : pl) {
		s->initResources();
	}

	while (!shut_down) {
#ifdef _WINDOWS
		Sleep(msecs);
#else
		sleep(msecs);
#endif
		bool wait = false;
		for (Stage* s : pl)
			wait |= !s->isDone();
		if (wait)
			continue;

		for (int i = 1; i < pl.size(); i++)
			pl[i]->receiveInputData(pl[i - 1]->retrieveOutputData());

		for (Stage* s : pl)
			s->signalWork();
	}


	//empty the pipeline
	for (int i = 0; i < pl.size(); i++) {
		bool ok = true;
		do {
			bool wait = false;
#ifdef _WINDOWS
			Sleep(msecs);
#else
			sleep(msecs);
#endif
			for (Stage* s : pl)
				wait |= !s->isDone();
			if (wait)
				continue;
			else
				ok = false;

			for (int j = i + 1; j < pl.size(); j++)
				pl[j]->receiveInputData(pl[j - 1]->retrieveOutputData());

			for (Stage* s : pl)
				s->signalWork();

		} while (ok);

		pl[i]->deallocateResources(); //shut down the stage
	}

	/*for (Stage* s : pl) {
		s->deallocateResources();
	}*/
}

void Pipeline::stop()
{
	shut_down = true;
}

std::string Pipeline::toString()
{
	std::string res("PIPELINE STAGES:\n");
	int count = 0;
	for (Stage* s : pl)
		res += "    [" + to_string(count++) + "] " + s->name + "\n";
	return std::string();
}

Pipeline::Stage::Stage(ptr<StageActions> actions, std::function<void()> sp) :
	name(actions->stage_name),
	a(actions),
	must_join(false),
	is_dead(false),
	t(&Stage::execute, this),
	stopPipeline(sp)
{
}

Pipeline::Stage::Stage(const Stage& s) :
	name(s.a->stage_name),
	a(s.a),
	must_join(false),
	is_dead(false),
	t(&Stage::execute, this)
{
}

Pipeline::Stage::~Stage() {
	deallocateResources();

#ifdef TIMER_ON
	cout << name << " time stats (min/avg/max): " << min_time << "/" << accumulator / toc_count << "/" << max_time << endl;
#endif // TIMER_ON
}

ptr<void> Pipeline::Stage::retrieveOutputData() {
	return outdata;
}

void Pipeline::Stage::receiveInputData(ptr<void> prev_stage_data) {
	indata = prev_stage_data;
}

void Pipeline::Stage::execute()
{
	if (is_dead)
		return;

	while (!must_join) {
		unique_lock<mutex> lck(mtx);
		cv.wait(lck);
		if (must_join)
			break; //necessary to tear down the stage on pipeline shut down
#ifdef TIMER_ON
		Tic t;
#endif // TIMER_ON

		done = false;
		bool end_of_input = false;
		outdata = a->execute(indata, end_of_input);
		done = true;

		if (end_of_input)
			stopPipeline();

#ifdef TIMER_ON
		double time = t.toc();
		toc_count++;
		if (time < min_time)
			min_time = time;
		if (time > max_time)
			max_time = time;
		accumulator += time;
#endif // TIMER_ON
	}
}

void Pipeline::Stage::initResources()
{
	if (is_dead)
		return;

	a->init();
}

void Pipeline::Stage::deallocateResources()
{
	if (is_dead)
		return;

	must_join = true;
	signalWork();
	if (t.joinable())
		t.join();

	a->deallocate();

	is_dead = true;
}

void Pipeline::Stage::signalWork() {
	unique_lock<mutex> lck(mtx);
	cv.notify_one();
}
