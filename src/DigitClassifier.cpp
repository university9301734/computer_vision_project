#include <stages.h>

using namespace cv;
using namespace std;

DigitClassifier::DigitClassifier(std::string cfg_file) {
	FileStorage fs(cfg_file, FileStorage::READ);
	if (!fs.isOpened())
		throw exception(("[DigitClassifier] Wrong config file: " + cfg_file).c_str());

	std::string path;

	fs["svm_digits_cfg"] >> path;
	fs["svm_look_for"] >> look_for;
	fs.release();

	mser = MSER::create();

	svm_digits = ml::SVM::load(path);
	if (svm_digits == nullptr || !svm_digits->isTrained())
		throw exception(("[DigitClassifier] Error loading digits SVM: " + path).c_str());

	hog = HOGDescriptor(
		Size(16, 16), //winSize
		Size(8, 8), //blocksize
		Size(4, 4), //blockStride,
		Size(8, 8), //cellSize,
		12, //nbins,
		1, //derivAper,
		-1, //winSigma,
		0, //histogramNormType,
		0.2, //L2HysThresh,
		1,//gammal correction,
		64,//nlevels=64
		1);//Use signed gradients

}

ptr<void> DigitClassifier::execute(ptr<void> prev_stage_output, bool& end_of_data) {
	if (prev_stage_output == nullptr)
		return nullptr;

	auto prev_stage = static_pointer_cast<SignClassificationResult>(prev_stage_output);

	auto& speed_rois = prev_stage->ROIs[look_for];
	auto& img = prev_stage->orig_img;
	Mat h, g;
	Mat ker = getStructuringElement(0, Size(2, 2), Point(-1, -1)); //create a rect kernel with central anchor

	vector<Point> all_digits_contours;

	cvtColor(img, h, CV_BGR2GRAY);
	for (const auto& sr : speed_rois) {
		int k = max(img.size().width, img.size().height); //need to use the entire image size here
		adaptiveThreshold(h(sr), g, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, ((int)(0.1 * k)) * 2 + 1, 0); //last parameter is an intensity offset

		morphologyEx(g, g, MORPH_OPEN, ker);
		morphologyEx(g, g, MORPH_CLOSE, ker);

		vector<vector<Point>> regions;
		vector<Rect> bb;
		mser->detectRegions(g, regions, bb);

		Rect digits_roi;
		if (!bb.empty()) {
			digits_roi = find_digits_bb(g.size(), bb);
			if (digits_roi.width == 0 || digits_roi.height == 0)
				continue;

			//copy the number at the center of a new image, but add a thin border of 6 px
			Mat d = Mat::ones(Size(max(digits_roi.width, digits_roi.height) + 6, max(digits_roi.width, digits_roi.height) + 6), CV_8U) * 255;
			Rect roi(
				(d.cols - g(digits_roi).cols) / 2,
				(d.rows - g(digits_roi).rows) / 2,
				g(digits_roi).cols,
				g(digits_roi).rows
			);
			g(digits_roi).copyTo(d(roi));
			resize(d, d, Size(32, 32)); //for svm
			bitwise_not(d, d); //for svm

			vector<float> descr;
			hog.compute(d, descr);
			Mat ds;
			for (const auto d : descr)
				ds.push_back(d);

			ds = ds.t();
			int detected_speed = (int)svm_digits->predict(ds);
			
#ifdef DEBUG
			int kkk = rand();			
			cv::imwrite("..\\OUTPUT\\" + to_string(kkk) + "_" + to_string(detected_speed) + "digits.jpg", d);
#endif

			if (detected_speed > 0) {
				prev_stage->ROIs[look_for + ": " + to_string(detected_speed)].push_back(sr);
			}
		}

	}

	prev_stage->ROIs[look_for].clear(); //clear raw inputs

	return prev_stage; //TODO: return svm classification too
}

Rect DigitClassifier::find_digits_bb(Size img_size, vector<Rect> all_bb, float delta) {

	Point2f center(img_size.width / 2, img_size.height / 2);
	vector<Point> pts;

	//find fist digit: the closest to the center
	float max_dist = (img_size.width * delta) * (img_size.width * delta);
	for (int i = 0; i < all_bb.size(); i++) {
		Rect& b = all_bb[i];
		//Point2f b_center(b.x / 2, b.y / 2);
		Point2f b_center(b.x, b.y);
		Point2f dist = center - b_center;
		float d2 = dist.x * dist.x + dist.y * dist.y; //use squared distance
		if (d2 < max_dist) {
			pts.push_back(Point(b.x, b.y));
			pts.push_back(Point(b.x + b.width, b.y + b.height));
		}
	}

	return boundingRect(pts);
}
