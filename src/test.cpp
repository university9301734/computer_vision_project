

/****************************************************************************************
*****************************************************************************************
***																					  ***
***								DEPRECATED AND NOT USED								  ***
***								left only for reference								  ***
***																					  ***
*****************************************************************************************
*****************************************************************************************/






#include <iostream>
#include <thread>

#include <pipeline.h>
#include <utils.h>
#include <ClassifierHelper.h>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <math.h>

using namespace std;
using namespace std::placeholders;
using namespace cv;

void gamma_transform(Mat& src_dst, float gamma = 0.01) {
	Mat fff;
	int flag = CV_32FC3;
	if (src_dst.channels() == 2)
		flag = CV_32FC2;
	else if (src_dst.channels() == 1)
		flag = CV_32FC1;

	src_dst.convertTo(fff, flag);
	cv::pow(fff, gamma, fff);
	fff = pow(255, 1 - gamma) * fff;
	for (int r = 0; r < fff.rows; r++)
		for (int c = 0; c < fff.cols; c++) {
			Vec3f px_bgr = fff.at<Vec3f>(r, c);
			auto px_b = cv::saturate_cast<uchar>(px_bgr[0]);
			auto px_g = cv::saturate_cast<uchar>(px_bgr[1]);
			auto px_r = cv::saturate_cast<uchar>(px_bgr[2]);
			src_dst.at<Vec3b>(r, c) = Vec3b(px_b, px_g, px_r);
		}
}

/*void bright_and_contrast(Mat& src_dst, float alpha = 1, float beta = 0) {
	src_dst = alpha * src_dst + beta;
}*/

string format(int i) {
	if (i < 10)
		return "0000" + to_string(i);
	else if (i < 100)
		return "000" + to_string(i);
	else if (i < 1000)
		return "00" + to_string(i);
	else if (i < 10000)
		return "0" + to_string(i);
	else
		return to_string(i);
}

shared_ptr<void> initColorSegmentationExperimental() {
	return shared_ptr<void>(new VideoCapture("..\\DATA\\img%02d.jpg"));
}


struct ColorSegmentationResultNamed : ColorSegmentationResult {
	std::string name;
};

struct ColorSegmentationResultRects : ColorSegmentationResult {
	std::vector<Rect> rois;
};

int n = 0;
shared_ptr<void> executeColorSegmentationExperimental(shared_ptr<void> prev_stage_output, shared_ptr<void> this_stage_data, bool& end_of_data) {

	//prev_stage_output not needed: this is the first stage

	Mat tmp, h, g, img_modifiable;
	/*auto cap = static_pointer_cast<VideoCapture>(this_stage_data);

	if (!cap->isOpened()) {
		end_of_data = true;
		return nullptr;
	}

	(*cap) >> img_modifiable;*/


	if (n > 2000) {
		end_of_data = true;
		return nullptr;
	}
	string name = "E:\\CV_proj_data\\negatives\\color\\" + format(n) + "n.jpg";
	img_modifiable = imread(name);
	n++;


	if (img_modifiable.empty()) {
		//end_of_data = true;
		return nullptr;
	}

	if (max(img_modifiable.cols, img_modifiable.rows) > 500)
		resize(img_modifiable, img_modifiable, Size(500, 500));

	const Mat img = img_modifiable.clone();

	cvtColor(img, h, CV_BGR2HLS);
	g = Mat::zeros(h.size(), CV_8U);

	int lowS = 125;

	//---------------------------------------------------------segment red-------------------------------------------------
	Scalar low1(0, 0, lowS), max1(15 / 2, 255, 255), low2((360 - 15) / 2, 0, lowS), max2(360 / 2, 255, 255);
	inRange(h, low1, max1, tmp);
	inRange(h, low2, max2, g);
	g += tmp;

	//--------------------------------------------------------segment blue---------------------------------------------------
	//-----step 1
	/*vector<Mat> bgr;
	split(img, bgr);
	for (int k = 0; k < 1; k++)
		equalizeHist(bgr[k], bgr[k]);
	merge(bgr, img_modifiable);
	cvtColor(img_modifiable, h, CV_BGR2HLS);

	Scalar blue_low((240 - 30) / 2, 255*0.25, 255 * 0.65), blue_max((240 + 30) / 2, 255 * 0.6, 255);
	inRange(h, blue_low, blue_max, tmp);

	//-----step 2
	Mat tmp2;
	img_modifiable = img.clone();
	bright_and_contrast(img_modifiable, 3, 0);
	cvtColor(img_modifiable, h, CV_BGR2HLS);

	inRange(h, blue_low, blue_max, tmp2);
	threshold(tmp + tmp2, tmp, 1, 200, THRESH_BINARY);
	g += tmp;*/


	Mat ker = getStructuringElement(0, Size(3, 3), Point(-1, -1)); //create a rect kernel with central anchor
	morphologyEx(g, g, MORPH_OPEN, ker);
	morphologyEx(g, g, MORPH_CLOSE, ker);

	//------------------------------------------
	bitwise_not(g, g);
	distanceTransform(g, g, DIST_L1, DIST_MASK_PRECISE);
	log(g + 1, g);
	g = ((255.0 / log(256))) * g;
	normalize(g, g, 0, 255, NORM_MINMAX);
	g.convertTo(g, CV_8UC1);
	g = 255 - g;
	//------------------------------------------

	auto ptr = shared_ptr<ColorSegmentationResultNamed>(new ColorSegmentationResultNamed);
	ptr->orig_img = img.clone();
	ptr->segmented_img = g.clone();
	ptr->name = name;
	return ptr;
}

void deallocateColorSegmentationExperimental(shared_ptr<void> data) {
	auto cap = static_pointer_cast<VideoCapture>(data);
	cap->release();
}

int ccc = 0;
std::shared_ptr<void> executeSaveImageExperimental(std::shared_ptr<void> prev_stage_output, std::shared_ptr<void> this_stage_data, bool& end_of_data) {
	if (prev_stage_output == nullptr)
		return nullptr;

	auto result = static_pointer_cast<ColorSegmentationResultRects>(prev_stage_output);

	Mat img = result->orig_img;
	if (img.empty())
		return nullptr;

	int i = rand();
	string n = "..\\OUTPUT\\" + to_string(i) + "res.jpg";

	for (auto& r : result->rois)
		rectangle(img, r, Scalar(0, 255, 0), 2);
	imwrite(n, img);
#ifdef DEBUG
	imwrite("..\\OUTPUT\\" + to_string(i) + "seg.jpg", result->segmented_img);
#endif // DEBUG

	return nullptr; //this is the last stage
}


shared_ptr<void> executeDistTransform(shared_ptr<void> prev_stage_output, shared_ptr<void> this_stage_data, bool& end_of_data) {

	//prev_stage_output not needed: this is the first stage

	Mat tmp, h, g, img_modifiable;
	/*auto cap = static_pointer_cast<VideoCapture>(this_stage_data);

	if (!cap->isOpened()) {
	end_of_data = true;
	return nullptr;
	}

	(*cap) >> img_modifiable;*/


	if (n > 2000) {
		end_of_data = true;
		return nullptr;
	}
	string name = "E:\\CV_proj_data\\warning\\negatives\\" + format(n) + "n.jpg";
	//string name = "E:\\CV_proj_data\\warning\\" + format(n) + "w.jpg";
	img_modifiable = imread(name, IMREAD_GRAYSCALE);
	n++;


	if (img_modifiable.empty()) {
		//end_of_data = true;
		return nullptr;
	}


	const Mat img = img_modifiable.clone();
	
	g = img_modifiable.clone();
	threshold(g, g, 210, 255, THRESH_BINARY);
	

	//------------------------------------------
	bitwise_not(g, g);
	distanceTransform(g, g, DIST_L1, DIST_MASK_PRECISE);
	log(g + 1, g);
	g = ((255.0 / log(256))) * g;
	normalize(g, g, 0, 255, NORM_MINMAX);
	g.convertTo(g, CV_8UC1);
	g = 255 - g;
	//------------------------------------------

	auto ptr = shared_ptr<ColorSegmentationResultNamed>(new ColorSegmentationResultNamed);
	ptr->orig_img = img.clone();
	ptr->segmented_img = g.clone();
	ptr->name = name;
	return ptr;
}


shared_ptr<void> executeDigitSegmentationExperimental(shared_ptr<void> prev_stage_output, shared_ptr<void> this_stage_data, bool& end_of_data) {

	//prev_stage_output not needed: this is the first stage

	if (this_stage_data == nullptr)
		return nullptr;

	Mat tmp, h, g, img_modifiable;
	auto cap = static_pointer_cast<VideoCapture>(this_stage_data);

	if (!cap->isOpened()) {
	end_of_data = true;
	return nullptr;
	}

	(*cap) >> img_modifiable;

	if (img_modifiable.empty()) {
		end_of_data = true;
		return nullptr;
	}

	vector<Mat> chs;
	split(img_modifiable, chs);
	for (auto& c : chs)
		equalizeHist(c, c);
	merge(chs, img_modifiable);

	const Mat img = img_modifiable.clone();

	cvtColor(img, h, CV_BGR2GRAY);
	g = Mat::zeros(h.size(), CV_8U);

	int lowS = 125;

	//---------------------------------------------------------segment red-------------------------------------------------
	/*Scalar low1(0, 128 - 10, 128 - 10), max1(50, 128 + 10, 128 + 10);
	inRange(h, low1, max1, g);*/
	//threshold(h, g, 230, 255, THRESH_BINARY);
	//inRange(h, Scalar(210), Scalar(255), g);
	
	int k = max(img.size().width, img.size().height);
	adaptiveThreshold(h, g, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, ((int)(0.1 * k)) * 2 + 1, -20);
	
	Mat ker = getStructuringElement(0, Size(2, 2), Point(-1, -1)); //create a rect kernel with central anchor
	morphologyEx(g, g, MORPH_OPEN, ker);
	morphologyEx(g, g, MORPH_CLOSE, ker);

	/*
	SimpleBlobDetector::Params params;
	params.blobColor = 255;
	params.filterByCircularity = true;
	params.minCircularity = 0.8;
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
	std::vector<KeyPoint> keypoints;
	detector->detect(g, keypoints);
	

	vector<Rect> rois;
	for (auto kp : keypoints) {
		float d = keypoints[0].size;
		Rect r(kp.pt - kp.pt / 2, Size(d, d));
		rois.push_back(r);
	}
	*/
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy; //
	findContours(g, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

	vector<Rect> rois;
	vector<Point> all_digits_contours;
	for (int l = 0; l < contours.size(); l++) {
		auto& c = contours[l];
		if (hierarchy[l][3] != -1 && hierarchy[l][2] == -1) //hierarchy[l][3] != -1 -> has a parent, hierarchy[l][2] == -1 -> no child contours
			for (auto& p : c)
				all_digits_contours.push_back(p);
			
	}
	rois.push_back(boundingRect(all_digits_contours));

	auto ptr = shared_ptr<ColorSegmentationResultRects>(new ColorSegmentationResultRects);
	ptr->orig_img = img.clone();
	ptr->segmented_img = g.clone();
	ptr->rois = rois;
	return ptr;
}

int main(int argc, char** argv) {

	Mat h, mm = imread("E:\\CVPROJ-Frizziero-1178973\\DATA\\img14.jpg", CV_LOAD_IMAGE_ANYCOLOR);
	cvtColor(mm, h, CV_BGR2HLS);
	h.convertTo(h, CV_32FC3);
	vector<Mat> chs;
	split(h, chs);
	Mat rr = chs[0].clone();
	pow(rr - (240 / 2), 2, chs[0]);
	merge(chs, h);
	//Scalar blue_low(0, 255 * 0.25, 255 * 0.65), blue_max(300, 255 * 0.6, 255);
	Scalar blue_low(0, 255 * 0.25, 255 * 0.4), blue_max(250, 255 * 0.6, 255);
	inRange(h, blue_low, blue_max, mm);
	imwrite("E:\\CVPROJ-Frizziero-1178973\\DATA\\img.jpg", mm);
	return 0;

	string folder = "E:\\CV_proj_data\\warning\\";
	string f1 = "00010w.jpg";
	string f2 = "00085w.jpg";
	string f3 = "00226w.jpg";

	Mat hu1, hu2, hu3;
	Moments mnts1 = moments(imread(folder + f1, CV_LOAD_IMAGE_GRAYSCALE), false);
	HuMoments(mnts1, hu1);
	Moments mnts2 = moments(imread(folder + f2, CV_LOAD_IMAGE_GRAYSCALE), false);
	HuMoments(mnts2, hu2);
	Moments mnts3 = moments(imread(folder + f3, CV_LOAD_IMAGE_GRAYSCALE), false);
	HuMoments(mnts3, hu3);
	cout << hu1 << endl;
	cout << hu2 << endl;
	cout << hu3 << endl;

	int i;
	cin >> i;
	return 0;

	StageActions stage0 = {
		"Color Segmentation",
		initColorSegmentationExperimental,
		executeDigitSegmentationExperimental,
		deallocateColorSegmentationExperimental,
	};

	StageActions stage1 = {
		"Save image",
		nullptr,
		executeSaveImageExperimental,
		nullptr
	};

	int Hz = 1000; //target hertz
	Pipeline* pipeline = new Pipeline(Hz);
	pipeline->appendStageAt(stage0, 0);
	pipeline->appendStageAt(stage1, 1);

	thread pipeline_thread(&Pipeline::run, pipeline);

	//TODO: consider doing other stuff in this thread, otherwise delete separate thread invocation for the pipeline
	//IDEA: set a 'cin' to listen for user input: if input --> shut down pipeline and program (useful during video???)

	if (pipeline_thread.joinable())
		pipeline_thread.join();

	delete pipeline;
	return 0;
}