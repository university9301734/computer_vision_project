#include <stages.h>

using namespace cv;
using namespace std;



SignClassifier::helper::helper(std::string& xml, std::string& name, std::string& params) :
	name(name)
{
	c = CascadeClassifier(xml);
	parseParams(params);
}

const vector<string> split_str(const string& s, const char& c)
{
	string buff{ "" };
	vector<string> v;

	for (auto n : s)
	{
		if (n != c) buff += n; else
			if (n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if (buff != "") v.push_back(buff);

	return v;
}

void SignClassifier::helper::parseParams(const string& p){
	//String format: "scaleFactor:<float>;minNeighbors:<int>;flags:<int>;minSize:<int>;maxSize:<int>"  NB: no spaces allowed
	vector<string> tokens = split_str(p, ';');
	scaleF = 1.1;
	minN = 3;
	flags = 0;
	minS = Size();
	maxS = Size();
	for (const auto& t : tokens) {
		vector<string> pair = split_str(t, ':');
		if (pair[0].compare("scaleFactor") == 0) {
			scaleF = stof(pair[1]);
		}
		else if (pair[0].compare("minNeighbors") == 0) {
			minN = stoi(pair[1]);
		}
		else if (pair[0].compare("flags") == 0) {
			flags = stoi(pair[1]);
		}
		else if (pair[0].compare("minSize") == 0) {
			int min = stoi(pair[1]);
			if (min > 0)
				minS = Size(min, min);
		}
		else if (pair[0].compare("maxSize") == 0) {
			int max = stoi(pair[1]);
			if (max > 0)
				maxS = Size(max, max);
		}
	}	
}



//-------------------------------------------------------------------------------------------------------------


SignClassifier::SignClassifier(string config_file)
{
	FileStorage fs(config_file, FileStorage::READ);
	if (!fs.isOpened())
		throw exception(("[SignClassifier] Wrong config file :" + config_file).c_str());
		//cout << "error reading config file" << config_file << endl;

	vector<string> xml, names, params;
	fs["classifier_xml_list"] >> xml;
	fs["classifier_name_list"] >> names;
	fs["classifier_params_list"] >> params;

	if (xml.size() != names.size())
		throw exception("[SignClassifier] Each configuration file in 'classifier_xml_list' must have a corresponding name in 'classifier_name_list'");

	if (xml.size() != params.size())
		throw exception("[SignClassifier] Each configuration file in 'classifier_xml_list' must have a corresponding list of params in 'classifier_params_list'");

	for (int i = 0; i < xml.size(); i++)
		classifiers.push_back(helper(xml[i], names[i], params[i]));
}

ptr<void> SignClassifier::execute(ptr<void> prev_stage_output, bool& reached_end_of_input) {
	if (prev_stage_output == nullptr)
		return nullptr;

	auto indata = static_pointer_cast<ColorSegmentationResult>(prev_stage_output);

	map<string, vector<Rect>> m;

	//TODO: THIS IS NOT A REALLY GOOD WAY. It would have been better to instantiate a thread pool in the constructor.
	//but then there is signaling overhead to manage.
	vector<thread> t_list(classifiers.size());
	
	int count = 0;
	for (auto& elem : classifiers) {
		m[elem.name]; //create an entry in the map, with same key as classifier's
		//elem.detect(indata->segmented_img, &m[elem.name]);
		t_list[count++] = thread(&helper::detect, elem, indata->segmented_img, &m[elem.name]);
	}

	for (auto& t : t_list)
		if (t.joinable())
			t.join();

	ptr<SignClassificationResult> res(new SignClassificationResult);
	res->orig_img= indata->orig_img;
#ifdef DEBUG
	res->segmented_image = indata->segmented_img;
#endif // DEBUG
	res->ROIs = m;
	return res;
}
