#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/objdetect.hpp>
#include <iostream>
#include <random>
#include <opencv2/ml.hpp>

using namespace cv;
#ifdef NOT_GENERATING_SVM_DATASET
#undef NOT_GENERATING_SVM_DATASET
#endif // NOT_GENERATING_SVM_DATASET


std::uniform_real_distribution<float> unif(0, 1);
std::default_random_engine engine;
std::vector<int> speeds = { 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130 };
std::string folder = "E:\\CV_proj_data\\svm\\dataset\\";

inline float U() { return unif(engine); };

namespace ns {

	void rotate(Mat& m) {
		float theta_deg = 25.0 * (-1.0 + 2.0 * U()); // -+45 deg
		Mat M = getRotationMatrix2D(Point2f(m.size() / 2), theta_deg, 1);
		warpAffine(m, m, M, m.size());
	}

	void skew(Mat& m) {

		Size old_s = m.size();
		float edge = m.size().width;

		Point2f src[] = { Point2f(0,0), Point2f(edge, 0), Point2f(edge, edge), Point2f(0, edge) };

		float a = 0.2 * U();
		float b = 0.2 * U();
		float c = 0.2 * U();
		float d = 0.2 * U();

		Point2f dst[] = {
			src[0] + Point2f(edge, edge) * a,
			src[1] - src[1] * b,
			src[2] - src[2] * c,
			src[3] - src[3] * d
		};

		Mat M = getPerspectiveTransform(src, dst);

		float width = max(dst[1].x - dst[0].x, dst[2].x - dst[0].x);
		float height = max(dst[3].y - dst[0].y, dst[2].y - dst[0].y);
		width = max(height, width);

		warpPerspective(m, m, M, Size(width*2, width*2));

		//recenter image
		Point2f old_center(edge / 2, edge / 2), x1(0, 0), x2(edge, edge);
		std::vector<Point2f> o, n;
		o.push_back(old_center);
		o.push_back(x1);
		o.push_back(x2);
		perspectiveTransform(o, n, M);
		Mat T = Mat::zeros(Size(3, 2), CV_64F);
		T.at<double>(0, 2) = -old_center.x + n[0].x;
		T.at<double>(1, 2) = -old_center.y + n[0].y;
		T.at<double>(0, 0) = 1;
		T.at<double>(1, 1) = 1;
		warpAffine(m, m, T, Size(width, width));

		resize(m, m, old_s);
	}

	void scale(Mat& m) {
		Mat M = getRotationMatrix2D(Point2f(m.size() / 2), 0, 0.5 + 0.5 * U()); //use only the translations to center the image, and the scale factor
		warpAffine(m, m, M, m.size());
	}

	void addNoise(Mat& m) {
		Size s = m.size();

		int i = round(U() * 10.0);
		for (int j = 0; j < i; j++) {
			Point p(U() * s.width, U() * s.height);
			int r = round(U() * 5.0);
			drawMarker(m, p, Scalar(255 * round(U())), round(U() * 6.9), round(U() * 3.0)); // randomly black or white, random marker style, random marker size
		}

	}

	void dilate(Mat& m) {
		int a = (int)(5 * U()) * 2 + 1;
		int b = (int)(5 * U()) * 2 + 1;

		Mat k = getStructuringElement(0, Size(a, b), Point(-1, -1));
		cv::dilate(m, m, k);
	}

	void erode(Mat& m) {
		int a = (int)(3 * U()) * 2 + 1;
		int b = (int)(3 * U()) * 2 + 1;

		Mat k = getStructuringElement(0, Size(a, b), Point(-1, -1));
		cv::erode(m, m, k);
	}

	void save(Mat& m, int speed, int count, int img_size = 32) {
		resize(m, m, Size(img_size, img_size));
		threshold(m, m, 120 + (int)(120 * (-0.1 + 2 * 0.1 *U())), 255, THRESH_BINARY);
		cv::imwrite(folder + std::to_string(speed) + "_" + std::to_string(count++) + ".jpg", m);
	}
}

void generate_dataset(int seed) {
	engine = std::default_random_engine(seed);
	//-------------------------configuration------------------------------
	std::vector<void (*)(Mat& m)> functions = { ns::rotate, ns::skew, ns::addNoise};
	int img_size = 128;
	int final_img_size = 32;
	float text_scale = 2; //at 2 --> speed 130 has 119px + text_thickness px of width
	float text_thickness = 4;
	Scalar text_color(255), bg_color(0);
	float randomness1 = 0.2, randomness2 = 0.5;
	int rep_per_speed = 30;

	std::vector<int> fonts = {FONT_HERSHEY_SIMPLEX, /*FONT_HERSHEY_PLAIN,*/ FONT_HERSHEY_DUPLEX, FONT_HERSHEY_SCRIPT_SIMPLEX};
	
	for (const int speed : speeds) {
		int count = 0;
		for (int r = 0; r < rep_per_speed; r++) {
			Mat m = Mat(Size(img_size, img_size), CV_8U, bg_color);

			int base = 0;
			int font = fonts[(int)floor(U() * fonts.size())];
			float ts = text_scale * (speed < 100 ? 1.4 : 1) + (-randomness1 + 2 * randomness1 * U());
			float tt = text_thickness * (speed < 100 ? 1.4 : 1) + (-randomness2 + 2 * randomness2 * U());
			Size text_size = cv::getTextSize(std::to_string(speed), font, ts, tt, &base);
			Point p(
				img_size / 2 - text_size.width / 2,
				img_size / 2 + text_size.height / 2
			);
			cv::putText(m, std::to_string(speed), p, font, ts, text_color, tt);
			ns::save(m, speed, count++, final_img_size);
			for (const auto f : functions) {
				Mat m2 = m.clone();
				f(m2);
				ns::save(m2, speed, count++, final_img_size);
			}

			for (int k = 0; k < 1; k++) {
				Mat m2 = m.clone();
				int idx1 = floor(U() * functions.size());
				int idx2 = floor(U() * functions.size());
				functions[idx1](m2);
				functions[idx2](m2);
				ns::save(m2, speed, count++, final_img_size);
			}
		}
	}

	//generate 'sink' class dataset: all data that are not a speed number
	for (int i = 0; i < rep_per_speed * 5; i++) {
		float p = U();
		Mat m = Mat(Size(img_size, img_size), CV_8U, bg_color);
		for (int k = 0; k < img_size; k++)
			for (int j = 0; j < img_size; j++)
				m.at<uchar>(k, j) = 255 * (p * 0.5 > U());

		ns::dilate(m);
		ns::addNoise(m);
		for (int n = 0; n < 15 * U(); n++) {
			if (p > U())
				cv::rectangle(m, Rect(Point2f(U() * img_size, U() * img_size), Point2f(U() * img_size, U() * img_size)), bg_color, (int)(U() * 15));
			if (1 - p > U())
				cv::circle(m, Point2f(U() * img_size, U() * img_size), (int)(U() * img_size), bg_color, (int)(U() * 15));
		}
		ns::save(m, -1, i, final_img_size);
	}
}



int main() {

	/*int base = 0;
	std::cout << " 1:130 " << cv::getTextSize(std::to_string(130), FONT_HERSHEY_DUPLEX, .5, 1, &base) << std::endl;
	std::cout << " 2:130 " << cv::getTextSize(std::to_string(130), FONT_HERSHEY_DUPLEX, 2, 3, &base) << std::endl;
	std::cout << " 3:130 " << cv::getTextSize(std::to_string(130), FONT_HERSHEY_DUPLEX, 3, 1, &base) << std::endl;
	std::cout << " 4:130 " << cv::getTextSize(std::to_string(130), FONT_HERSHEY_DUPLEX, 4, 1, &base) << std::endl;
	std::cout << " 1:60 " << cv::getTextSize(std::to_string(60), FONT_HERSHEY_DUPLEX, .5, 1, &base) << std::endl;
	std::cout << " 2:60 " << cv::getTextSize(std::to_string(60), FONT_HERSHEY_DUPLEX, 2, 1, &base) << std::endl;
	std::cout << " 3:60 " << cv::getTextSize(std::to_string(60), FONT_HERSHEY_DUPLEX, 3, 1, &base) << std::endl;
	std::cout << " 4:60 " << cv::getTextSize(std::to_string(60), FONT_HERSHEY_DUPLEX, 4, 1, &base) << std::endl;
	*/



	if (false) {

		std::cout << "seed : ";
		int seed;
		std::cin >> seed;

		generate_dataset(seed);

		return 0;

	}



	
	
	HOGDescriptor hog(
		Size(16, 16), //winSize
		Size(8, 8), //blocksize
		Size(4, 4), //blockStride,
		Size(8, 8), //cellSize,
		12, //nbins,
		1, //derivAper,
		-1, //winSigma,
		0, //histogramNormType,
		0.2, //L2HysThresh,
		1,//gammal correction,
		64,//nlevels=64
		1);//Use signed gradients

	

	if (true) {
		Mat train_data;
		Mat train_labels;

		speeds.push_back(-1); //sink class
		for (const auto s : speeds) {
			int max_count = 100;
			for (int count = 0; count < max_count; count++) {
				Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(count) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);
				std::vector<float> descriptor;
				hog.compute(m, descriptor);
				Mat in;
				for (const auto d : descriptor)
					in.push_back(d);

				train_data.push_back(in.t());


				Mat l(Size(1, 1), CV_32S, s);
				train_labels.push_back(l);
			}
		}

		/*
		Scalar _mean, _std;
		meanStdDev(train_data, _mean, _std);
		double mean = _mean[0], std = _std[0];

		train_data -= mean;
		train_data /= std;
		*/

		Ptr<ml::TrainData> data = ml::TrainData::create(train_data, ml::ROW_SAMPLE, train_labels);

		Ptr<ml::SVM> svm = ml::SVM::create();
		svm->setType(ml::SVM::C_SVC);
		svm->setKernel(ml::SVM::RBF);
		svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 1000, 1e-6));

		svm->trainAuto(data);

		svm->save(folder + "..\\svm_digits.yml");



		Mat test_data;
		Mat test_labels;

		for (const auto s : speeds) {
			int max_count = 150;
			for (int count = 100; count < max_count; count++) {
				Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(count) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);

				std::vector<float> descriptor;
				hog.compute(m, descriptor);
				Mat in;
				for (const auto d : descriptor)
					in.push_back(d);

				test_data.push_back(in.t());

				Mat l(Size(1, 1), CV_32S, s);
				test_labels.push_back(l);
			}
		}

		/*train_data -= mean;
		train_data /= std;*/




		Ptr<ml::SVM> test_svm = cv::ml::SVM::load(folder + "..\\svm_digits.yml");


		float train_err = test_svm->calcError(data, false, noArray());
		float test_err = test_svm->calcError(ml::TrainData::create(test_data, ml::ROW_SAMPLE, test_labels), false, noArray());

		std::cout << "train errors: " << train_err  << "%" << std::endl;
		std::cout << "test errors: " << test_err  << "%" << std::endl;

		/*std::cout << "mean: " << mean << std::endl;
		std::cout << "std: " << std << std::endl;*/

		int ccc;
		std::cin >> ccc;
		return 0;

	}
	else {

		Mat train_data;
		Mat train_labels;

		for (const auto s : speeds) {
			int max_count = 150;
			for (int count = 0; count < max_count; count++) {
				Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(count) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);
				std::vector<float> descriptor;
				hog.compute(m, descriptor);
				Mat in;
				for (const auto d : descriptor)
					in.push_back(d);

				train_data.push_back(in.t());
			}

			train_labels = Mat::ones(train_data.rows, 1, CV_32S);

			Ptr<ml::TrainData> data = ml::TrainData::create(train_data, ml::ROW_SAMPLE, train_labels);

			Ptr<ml::SVM> svm = ml::SVM::create();
			svm->setType(ml::SVM::ONE_CLASS);
			svm->setKernel(ml::SVM::RBF);
			svm->setGamma(0.1);
			//svm->setDegree(2);
			//svm->setCoef0(1);
			svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER + TermCriteria::EPS, 1000, 1e-6));
			svm->setNu(0.5);
			svm->train(data);

			svm->save(folder + "..\\svm_filter" + std::to_string(s) +".yml");



			Mat test_data;
			Mat test_labels;

			/*for (const auto s1 : speeds) {
				int max_count = 150;
				for (int count = 0; count < max_count; count++) {
					Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(count) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);

					std::vector<float> descriptor;
					hog.compute(m, descriptor);
					Mat in;
					for (const auto d : descriptor)
						in.push_back(d);

					test_data.push_back(in.t());

					Mat l(Size(1, 1), CV_32S, s1 == s ? 1 : 0);
					test_labels.push_back(l);
				}
			}*/

			for (int count = 100; count < max_count; count++) {
				Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(count) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);

				std::vector<float> descriptor;
				hog.compute(m, descriptor);
				Mat in;
				for (const auto d : descriptor)
					in.push_back(d);

				test_data.push_back(in.t());

				Mat l(Size(1, 1), CV_32S, 1);
				test_labels.push_back(l);
			}

			int k = 0;
			int kk = 0;
			while (k < 50) {
				auto s1 = speeds[kk++ % speeds.size()];
				if (s1 == s)
					continue;

				Mat m = imread(folder + std::to_string(s) + "_" + std::to_string(k) + ".jpg", CV_LOAD_IMAGE_GRAYSCALE);

				std::vector<float> descriptor;
				hog.compute(m, descriptor);
				Mat in;
				for (const auto d : descriptor)
					in.push_back(d);

				test_data.push_back(in.t());

				//Mat l(Size(1, 1), CV_32S, 0);
				test_labels.push_back(0);
				k++;				
			}





			Ptr<ml::SVM> test_svm = cv::ml::SVM::load(folder + "..\\svm_filter" + std::to_string(s) + ".yml");

			float train_err = test_svm->calcError(data, false, noArray());
			float test_err = test_svm->calcError(ml::TrainData::create(test_data, ml::ROW_SAMPLE, test_labels), false, noArray());

			std::cout << "SPEED: " + std::to_string(s) << std::endl;
			std::cout << "train error: " << train_err /*/ res_train.rows*/ << "%" << std::endl;
			std::cout << "test error: " << test_err /*/ res_test.rows*/ << "%" << std::endl;
			std::cout << std::endl;
		}

		int ccc;
		std::cin >> ccc;
		return 0;
	}








	
}