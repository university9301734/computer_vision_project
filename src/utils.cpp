#include <stages.h>

using namespace cv;
using namespace std;


ColorSegmentation::ColorSegmentation(std::string cfg) {
	FileStorage fs(cfg, FileStorage::READ);
	if (!fs.isOpened())
		throw exception(("[ColorSegmentation] cannot read configuration file: "+ cfg).c_str()); 

	std::string pattern = fs["input_video_or_img_sequence_pattern"];
	fs.release();

	cap = VideoCapture(pattern);
	if (!cap.isOpened())
		throw exception(("[ColorSegmentation] cannot open the image source: " + pattern).c_str());
}

ptr<void> ColorSegmentation::execute(ptr<void> prev_stage_output, bool& end_of_data) {
	Mat tmp, h, g, img_modifiable;

	cap >> img_modifiable;

	if (img_modifiable.empty()) {
		end_of_data = true;
		return nullptr;
	}

	const Mat img(img_modifiable);

	cvtColor(img_modifiable, h, CV_BGR2HLS);

	int lowS = 125;

	//--------------------------------------------------------segment red----------------------------------------------------
	/*Scalar low1(0, 0, lowS), max1(20 / 2, 255, 255), low2((360 - 20) / 2, 0, lowS), max2(360 / 2, 255, 255);
	inRange(h, low1, max1, tmp);
	inRange(h, low2, max2, g);
	g += tmp;*/

	Mat r;
	h.convertTo(r, CV_32FC3);
	vector<Mat> chs;
	split(r, chs);
	chs[0] += 90;
	subtract(chs[0], 180, chs[0], chs[0] > 180);
	pow(chs[0] - 90, 2, chs[0]);
	merge(chs, r);
	Scalar low1(0, 0, 125), max1(300, 255, 255);
	inRange(r, low1, max1, g);


	//--------------------------------------------------------segment blue---------------------------------------------------

	Mat b, b_tmp;
	h.convertTo(b, CV_32FC3);
	split(b, chs);
	pow(chs[0] - (240 / 2), 2, chs[0]);
	merge(chs, b);
	Scalar blue_low(0, 255 * 0.25, 255 * 0.4), blue_max(250, 255 * 0.6, 255);
	inRange(h, blue_low, blue_max, b_tmp);
	threshold(b_tmp, b_tmp, 1, 200, THRESH_BINARY);
	g += b_tmp;

	//-----step 1
	/*vector<Mat> bgr;
	split(img, bgr);
	for (int k = 0; k < 1; k++)
		equalizeHist(bgr[k], bgr[k]);
	merge(bgr, img_modifiable);
	cvtColor(img_modifiable, h, CV_BGR2HLS);

	Scalar blue_low((240 - 30) / 2, 255 * 0.25, 255 * 0.65), blue_max((240 + 30) / 2, 255 * 0.6, 255);
	inRange(h, blue_low, blue_max, tmp);

	//-----step 2
	img_modifiable = img.clone();
	bright_and_contrast(img_modifiable, 3, 0);
	cvtColor(img_modifiable, h, CV_BGR2HLS);

	Mat tmp2;
	inRange(h, blue_low, blue_max, tmp2);
	threshold(tmp + tmp2, tmp, 1, 200, THRESH_BINARY);
	g += tmp;*/
	
	//------------------------------------------filter a bit of noise------------------------------------------
	Mat ker = getStructuringElement(0, Size(3, 3), Point(-1, -1)); //create a rect kernel with central anchor
	morphologyEx(g, g, MORPH_OPEN, ker);
	morphologyEx(g, g, MORPH_CLOSE, ker);

	auto _ptr = ptr<ColorSegmentationResult>(new ColorSegmentationResult);
	_ptr->orig_img = img.clone();
	_ptr->segmented_img = g.clone();
	return _ptr;
}

ColorSegmentation::~ColorSegmentation() {
	cap.release();
}


SaveResults::SaveResults(string cfg) :
	counter(0)
{
	FileStorage fs(cfg, FileStorage::READ);
	if (!fs.isOpened())
		throw exception(("[SaveResults] cannot read configuration file: " + cfg).c_str());

	pattern = fs["output_video_or_img_sequence_pattern"];
	double fps = fs["video_fps"];
	Size video_frame_size(fs["video_frame_width"], fs["video_frame_height"]);
	fs.release();

	isVideo = pattern.find(".mp4") != string::npos; //string::npos == not found

	if (isVideo) {
		//only support mp4 video for now
		vw = VideoWriter(pattern, VideoWriter::fourcc('M', 'P', '4', 'V'), fps, video_frame_size);
		if (!vw.isOpened())
			throw exception(("[SaveResults] cannot open the video writer for path " + pattern).c_str());

#ifdef DEBUG
		seg = VideoWriter("..\\OUTPUT\\video_seg.mp4", VideoWriter::fourcc('M', 'P', '4', 'V'), fps, video_frame_size);
#endif // DEBUG
	}

	colors = { Scalar(0, 255, 0), Scalar(0, 170, 255), Scalar(255, 255, 0), Scalar(255, 0, 255), Scalar(255, 0, 0), Scalar(0, 0, 255) };
}

ptr<void> SaveResults::execute(ptr<void> prev_stage_output, bool& end_of_data) {
	if (prev_stage_output == nullptr)
		return nullptr;

	auto result = static_pointer_cast<SignClassificationResult>(prev_stage_output);

	Mat img = result->orig_img;
	if (img.empty())
		return nullptr;

	auto color = colors.begin();
	for (const auto roi_list : result->ROIs) {		
		for (const auto roi : roi_list.second) {
			rectangle(img, roi, *color, 2);
			int bl = 0;
			float fscale = 1;
			Size tSize = getTextSize(roi_list.first, FONT_HERSHEY_PLAIN, fscale, 1, &bl);
			while (tSize.width < roi.width) { fscale *= 1.1; tSize = getTextSize(roi_list.first, FONT_HERSHEY_PLAIN, fscale, 1, &bl);}
			rectangle(img, Rect(Point(roi.x, roi.y), tSize), *color, FILLED);
			putText(img, roi_list.first, Point(roi.x, roi.y + tSize.height), FONT_HERSHEY_PLAIN, fscale, Scalar(0,0,0));			
		}
		if (color != colors.end())
			color++;
	}

	if (isVideo){
		vw << img;

#ifdef DEBUG
		if (seg.isOpened())
			seg << result->segmented_image;
#endif // DEBUG
	}
	else {
#ifdef _WINDOWS
		char buff[256]; //max Windows path length
		sprintf_s(buff, 256, pattern.c_str(), counter++);
		string n(buff);
#else
		char buff[256]; //max Linux path length ???
		sprintf(buff, pattern.c_str(), counter++);
		string n(buff);
#endif // _WINDOWS

		if (!imwrite(n, img))
			cout << "[SaveResults] could not save image " << n << endl;

#ifdef DEBUG
		string d = "..\\OUTPUT\\" + to_string(rand()) + "dbg.jpg";
		cv::imwrite(d, result->segmented_image);
#endif // DEBUG
	}

	return nullptr; //this is the last stage
}

SaveResults::~SaveResults() {
	if (isVideo)
		vw.release();

#ifdef DEBUG
	if (isVideo) {
		if (seg.isOpened())
			seg.release();
	}
#endif // DEBUG
}
