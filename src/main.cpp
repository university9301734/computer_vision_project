#include <typedef.h>
#include <iostream>
#include <thread>

#include <pipeline.h>
#include <stages.h>
#include <csignal>


using namespace std;
using namespace std::placeholders;


int main(int argc, char** argv) {

	std::string cfg_file = argc == 1 ? "..\\DATA\\config_default.yml" : string(argv[1]);
	ptr<StageActions> stage0, stage1, stage2, stage3;
	
	try {
		stage0 = ptr<ColorSegmentation>(new ColorSegmentation(cfg_file));
		stage0->stage_name = "Input and color segmentation";
	
		stage1 = ptr<SignClassifier>(new SignClassifier(cfg_file));
		stage1->stage_name = "Road sign detection";			

		stage2 = ptr<DigitClassifier>(new DigitClassifier(cfg_file));
		stage2->stage_name = "Digit classification";

		stage3 = ptr<SaveResults>(new SaveResults(cfg_file));
		stage3->stage_name = "Results output";
	}
	catch(exception e)
	{
		cout << e.what() << endl << "Press any key to quit";
		int i;
		cin >> i;
		return 1;
	}

	int Hz = 50; //target hertz
	Pipeline* pipeline =  new Pipeline(Hz);
	pipeline->appendStageAt(stage0, 0);
	pipeline->appendStageAt(stage1, 1);
	pipeline->appendStageAt(stage2, 2);
	pipeline->appendStageAt(stage3, 3);

	/*
	thread pipeline_thread(&Pipeline::run, pipeline);

	//TODO: consider doing other stuff in this thread, otherwise delete separate thread invocation for the pipeline
	//IDEA: set a 'cin' to listen for user input: if input --> shut down pipeline and program (useful during video???)

	if (pipeline_thread.joinable())
		pipeline_thread.join();
	*/

	pipeline->run();

	delete pipeline;


	cout << "DONE. PRESS ANY KEY + <enter> TO QUIT." << endl;
	int i;
	cin >> i;
	return 0;
}