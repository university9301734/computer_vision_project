\documentclass[a4paper, twoside, 10pt]{article}

\usepackage[a4paper, left=2cm, right=2cm, top=3cm, bottom=3cm]{geometry}
\usepackage{graphicx}

\begin{document}

\begin{titlepage}
	\centering
	\includegraphics[width=0.5\hsize]{logo_unipd_dei.png}\par\vspace{1cm}
	\vspace{2cm}
	{\scshape\LARGE Università di Padova \par}
	\vspace{1cm}
	{\scshape\Large Report of final project\par}
	\vspace{1.5cm}
	{\huge\bfseries Computer Vision\par}
	\vspace{2cm}
	{\Large\itshape Ludovico Frizziero - 1178973\par}
	\vfill
	
	
	% Bottom of the page
	{\large \today\par}
\end{titlepage}
\newpage

\section{Introduction}

Goal of the project is to recognise certain types of road signals in images. The required types are:
\begin{itemize}
	\item no parking signs
	\item warning signs
	\item speed signs
\end{itemize}
For the speed signs, it is also required to tell the speed limit within it.

In order to recognise such signals, this project focuses on the most generic aspects that can characterize them, namely, their shape. This is because road signs have very variegate content, in particular the warning signs, but all share a certain shape depending on their role. Only if required, a more deep level of understanding is applied to certain types of signs, namely the speed ones.

\section{Code structure}
Before describing the object recognition process, I will describe how the code is structured and how it works, in order to have a better understanding of what will follow.

To quickly deploy and test ideas, without harming already functioning ones, but also to achieve multiple levels of understanding to the image, the code was structured with modularity in mind. Also, to try achieve faster performances, multithreading has found extensive use. The first part of the project was therefore aimed at developing a smart pipeline structure. 

\subsection{Pipeline}
The pipeline is an ordered collection of independent stages, which works in cycles. Each stage operates on the data outputted on the previous cycle by the preceding stage, and it is allocated in its own separate thread. Stages can be set at will during the pipeline creation, with whatever order is needed. The pipeline only enforces a stage to implement an executable method. Thanks to the multithreaded structure, a cycle is defined as the maximum between the duration of the slowest stage and the target periodicity, that can be set during the pipeline setup. Currently the target frequency of the pipeline is set high enough to make the slowest stage runtime define the cycle duration. As any pipeline, this helps reduce the execution time, but also increases a lot the actual throughput at the output stage.

\subsection{Stage} 
To enforce the implementation of the executable method, the pipeline defines an abstract struct called StageActions which has to be extended by stage classes, with a pure virtual method named $execute$. This method takes as input the data coming from the preceding stage, plus a signalling flag, which enables to shut down the pipeline if needed, and outputs the data elaborated from the relative stage. All data are embedded into smart (shared) pointers to avoid copying them around, but also to ease a stage, or even worse the pipeline, to have to deallocate the data once they are no longer needed, since understanding when this happen in a modular structure may not be always easy. The smart pointers by definition take care of this aspect, therefore they're an easy and powerful solution to the memory management problem. The StageActions struct also defines two auxiliary virtual methods, but not pure virtual, called $init$ and $deallocate$. These ended up to be never used in the project, but they were thought to be used to manage some data external to the stage object, and not known during the first stage initialization (i.e. in the stage's constructor).

The pipeline doesn't enforce any protocol/common interface to exchange data between two contiguous stages, since the shared pointers are all defined as 'void' type. Therefore the stages must know how to format output data in order for the next stage to understand them. The data formats used by the stages for this project can be found in $datacontainers.h$.


\subsection{Project's implementation of the pipeline}
For this project a total of four stages are used. The first is responsible to load images from the specified source, then to apply some preprocessing to it. The second is responsible to find the three types of road signals in the image. The third understands the speed limit for the speed signals. The fourth saves to disk the results. 

Each stage is configurable by some parameters, to be specified in a configuration file. Therefore the whole executable either does not need any command line argument (is such case it tries to load a default configuration), or it takes just a single argument: the (relative) path to the configuration file.

\begin{figure}[h]
\includegraphics[width=6cm]{pipeline.png} \includegraphics[width=12cm]{StageActions.PNG}
\caption{Pipeline representation, and the StageAction struct. The type ptr<T> is just an alias for std::shared\_ptr<T>.}
\end{figure}

\section{Image preprocessing}
The first stage, apart from loading the images, applies some preprocessing. It specifically tries to strip down every non useful feature from the image, to simplify the detection process by a machine learning algorithm. As already mentioned, a relevant feature for road signals is their shape, rather than their whole aspect. Also the colour is relevant, but since detection algorithm typically work on gray scale images or some type of feature, this information must be preserved by other means. The colour information is therefore used to segment the image. Since the required road signs exhibits strong components of red and blue, which are usually also very saturated, by searching for these information in the images, the stage highlights the shape of the road signals, while disregarding their content and significantly reducing the background noise. 

To segment the colours, the HSL colour space is used. This colourspace allows to easily separate colours, which are represented by angles, and at the same time gives a bit more robustness to the detection in different light conditions, thanks to the L component, although it is far from being really robust. To achieve better separation among colours, a non linear threshold is also used. The following transformation is applied to each pixel of the H channel
$$
s = (\alpha - r)^2 \le T
$$

where $r$ is the pixel value, $\alpha$ the reference colour angle (for instance the blue is $\alpha = 245$) and $T$ is the threshold value. Red colour needs a bit more care, since its reference value is 0, but the reasoning is the same.

Finally some opening and closing operations are applied to reduce the noise. The resulting mask has pixel valued 255 where red was detected, and 200 where blue was found, and this is also the output of the stage.

\begin{figure}[h]
	\centering
	\includegraphics[height=7cm]{color_seg.jpg} \includegraphics[height=7cm]{color_seg2.jpg}
	\caption{Examples of colour segmentation.}
\end{figure}

\newpage
\section{Sign detection}
The second stage detects the three types of sign (although other classifiers can easily be added by configuration file). Various techniques were evaluated to this end, but right from the start was clear that a machine learning capable algorithm was needed, since too much variation on signs angle of view and transformations can not be accounted for by ad hoc algorithms. Convolutional neural networks where not considered since OpenCV lacks an 'in-house' complete support for them (it is only able to load externally trained CNNs). Between SVM and Cascade Classifiers, the final choice ended up on the latters. 

The stage therefore uses three Cascade Classifier, each one trained to recognise one specific sign. Various combinations of training parameters where evaluated and tested. The best choices usually involved LBP weak features. Because the vision of the world the classifiers are faced with is very simplistic, during training a particular attention to not go into overfitting had to be made. In particular, a somewhat low number of positive example (around 80) helped with this aspect: classifiers trained with such parameter outperformed the ones that were trained on much higher positive sample count.

Moreover, to avoid as much as possible one classifier mistakenly identify a sign that belongs to one of the others, the negative examples contained cross references of signs, other than some random background noise.

Each region of interest detected by each of the classifier is then labelled accordingly and outputted by the stage.

\section{Speed limit recognition}
The third stage takes from the output of the previous stage only the regions of interest (ROI) labelled as "speed" (the label can be set in the configuration file), plus the original colored image. For each ROI, it takes the corresponding patch on the image, and converts it to grayscale. The aim is to segment the speed digits to classify them. Since they are always black, they correspond approximately to minima in the grayscale image. To better separate these minima, an adaptive threshold with somewhat high filter aperture is applied to the patch.

At this point the pach has been transformed into a mask. On this mask the MSER blob detector is used to find all minima locations. Then to identify the actual digits bounding box, a simple heuristic is used: digits are likely to be near the center of the patch. All such location that are within a threshold are grouped and a collective bounding box is found. Although simple, this heuristic proves to be effective. The (binary) patch under the bounding box is extracted and rescaled to 32x32 pixels. The HOG feature descriptor is then used to describe this little image. This description is the input of an SVM.

The SVM is trained on an artificially created dataset using default OpenCV's fonts. It has one class for each speed in the range $20, 30 ... 130$, plus a 'sink' class, called $-1$, which is used to tell that the input doesn't have speed digits within it, but rather it is random noise. The dataset comprises perspective-distorted views as well as affine transformations for each possible speed. The sink class is needed since the speed classifier finds each sign that just has a red circular rim, regardless of the content of the sign.

During validation on the synthetic dataset the SVM shows an accuracy of $96\% \sim 97\%$, and indeed it performs well also during the execution of the pipeline. It shows problems with the speed limit $30$, though, very easily misclassified as $80$. I think this problem is due to the high similarity of the font respectively used for 3 and for 8 in the road sings, while OpenCV has a more clear separation among such digits. I wasn't able to build in time a hard mined dataset for this particular problem though. Another shrewdness to help the SVM correctly classify the speed limits, is to make sure there always is a thin black border ($1\sim2$ px) between the digits and the image boundaries in the 32x32 pixel image. This avoids to loose the gradient information at the digits' corners when computing HOG features, other than to make the 32x32 image more similar to the ones in the synthetic dataset.

Each ROI labelled "speed" by the second stage is then updated: if a speed limit was found, the ROI is kept and simply relabelled to account for the speed, otherwise if the sink class was detected, the ROI is discarded.


\begin{figure}[h]
	\centering
	\includegraphics[height=1cm]{d1.jpg} \includegraphics[height=1cm]{d3.jpg}  \includegraphics[height=1cm]{d2.jpg} \includegraphics[height=1cm]{d5.jpg} \includegraphics[height=1cm]{d4.jpg}
	\caption{Examples of digit segmentation. The first two of them are correctly labelled as sink class, while 30 is labelled as 80. The other two are correctly classified too.}
\end{figure}

\section{Output}
The fourth and last stage just saves the result of the third stage. On top of that draws labelled bounding boxes on the coloured image. it can either save single images as a sequence, or a video in the mp4 format only, depending on the configuration parameters. Should other needs arise, more output stages can be easily attached to the pipeline.

\section{Final considerations}
The aim I pursued with this project was to try as much different techniques as possible. First of all, aside from color + generic shape, I tried to considered other possible representations of the world to submit to a learning algorithm. For instance I evaluated also the use of Hu Moments, or more in general image moments, since I was working with defined shapes, but I rejected them because they are not robust to perspective transformations, but most of all noise: segmentation is never perfect, and often has missing parts. Such moments are too sensible to these aspects. I also pondered the use of SVM instead of Cascade Classifiers also for sign detection, but rejected it because otherwise stages one and two would have ended up being too similar to stage three, in the procedure of locating the right image's regions where relevant content is, and then classifying it.

Did the project succeeded fully? The results provided by the project are good, but far from perfect: there are some weakpoints that needs to be tailored. First of all colour segmentation is too sensitive to image illumination, as already stated. This requires much more attention. Secondly, Cascade Classifiers show some problems in recognising some patterns, therefore hard mining of outliers / false positives needs to be implemented, to strengthen their training. 

\begin{figure}[h]
	\centering
	\includegraphics[height=6cm]{res08.jpg} \includegraphics[height=6cm]{res37.jpg}  \includegraphics[height=6cm]{res48.jpg} 
	\caption{Examples of final results. Some errors are present.}
\end{figure}

\end{document}          
